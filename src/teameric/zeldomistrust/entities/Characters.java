package teameric.zeldomistrust.entities;

import static teameric.zeldomistrust.items.Items.*;

/**
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider, Dwight Collier
 */
public class Characters {

    /**
     * The character Albina Maksim
     * @author Dwight Collier, Eric Schneider
     */
    public static Character albina = new Character("Albina Maksim", "Albina hails from Russia.\nAlbina is a grand swordswoman, and one of the strongest women in all of Russia.\nShe was a proud warrior, until one day she was accused of cheating, as everyone had begun to believe that her strength was all a front.\nAlbina went into a deep depression, and as no one trusted her anymore, she started to believe she was not longer worthy of the title \"Mother Russia\" anymore.\nThis journey, if you choose to add her, will allow her to prove herself to the people of Russia.", 100, 10);

    /**
     * The character Grigorii Liliya
     * @author Dwight Collier
     */
    public static Character grigorii = new Character("Grigorii Liliya", "Grigorii hails from China, and at one time the Shrine Grigorii protected was at peace.\nSo much so that Grigorii never felt the need to become stronger because, well, who in this world would destroy the Shrine of his people?\nThe same Shrine that passed down the message of happiness from generation to generation.\nObviously he had to be introduced to the Demon Lord Apocalypse.\nApocalypse saw the shrine as an obstacle and obliterated it, and all Grigorii cared for was gone.\nSeeking vengeance, Grigorii is anxious to join you on your quest.", 350, 5);

    /**
     * The character Dipti Saraswati
     * @author Dwight Collier
     */
    public static Character dipti = new Character("Dipti Saraswati", "Dipti hails from India. Even as a young man Dipti did drugs.\nDipti couldn’t stop using drugs; they immediately just became a need for him.\nHe couldn’t get over his addiction no matter how hard he tried.\nHe would always go and get his drugs.\nThere was no specific name for it, but everyone just referred to it as \"That good sh*t\".\nOne day Dipti's drug addiction almost led to his death and it ruined his relationship with his family.\nAfter that day, Dipti sought redemption... (and secretly better drugs).\nDipti will join you to redeem himself.", 50, 6);

    /**
     * The character Scourge Lightspeed
     * @author Dwight Collier
     */
    public static Character scourge = new Character("Scourge Lightspeed", "Scourge is a young man, he comes from America.\nHe lost his parents to the Demon Lord and before that day his name was Danny.\nWhy the change in name?\nNo parents = no rules, so he named himself after a name he thought a transformer would have.\nYears later he regretted that decision because Scourge Lightspeed is now on all of his official documentation.\nHe now curses the Demon Lord for taking away his parents who would have stopped him from making that dumb decision.\nHe will join you to stop the Demon Lord once and for all.", 250, 8);

    /**
     * The character Dope D' Chillin
     * @author Dwight Collier
     */
    public static Character dope = new Character("Dope D' Chillin", "Chillin felt like a king all of his life, he had all he could have wanted.\nHe had the ladies, money, and most of all, those sick beats.\nChillin is an African Rapper. Life was good until the Demon Lord came around and messed up his house.\nYou don’t just do that to a man who was chillin.\nCrashing his house was messed up and now Chillin is ready to spit fire and give the Demon lord that L while also giving him his Diss track.\nBTW, Chillin has a habit of rapping when he talks... it's kind of cool at first but gets old quick.\nChillin is ready to give you his mixtape.", 460, 5);

    /**
     * The character Fritzi Vreni
     * @author Dwight Collier
     */
    public static Character fritzi = new Character("Fritzi Vreni", "Fritzi was an unstoppable German wrestler.\nHe said it was \"100 percent German power.\"\nWhen Fritzi was in the worlds spotlight, everyone knew the name Fritzi Vreni.\nNo wrestler who heard his name didn’t tremble in fear, shaking in their boots.\nFritzi loved the attention and was happy that he had it, until the Demon Lord’s army scared his audience away.\nNow In order to regain the attention he loved, he vowed to defeat the Demon Lord.\nHe will join you for glory and to be an attention hog.", 500, 8);

    /**
     * The character Judoc Gwenneg
     * @author Dwight Collier
     */
    public static Character judoc = new Character("Judoc Gwenneg", "Judoc was a mother and a fencer.\nAs a fencer Judoc found almost no one that could oppose her.\nAnd as a mother you couldn’t find anyone who cared more for her child.\nWhen the Demon Lord rose, her town was one of the first to be attacked.\nShe realized she was no match for the demon army and hid, they almost succeeded in hiding with no problems but her crying baby gave them away.\nThey were caught and the child was killed, and now she seeks vengeance.", 300, 8);

    /**
     * The character Bile Skanda
     * @author Dwight Collier
     */
    public static Character bile = new Character("Bile Skanda", "Bile is a very LARGE man.\nNothing better than a good meal as Bile would always say.\nHe ate almost all of the time, and since there was a food surplus Bile couldn’t have been in a better position.\nHowever the Demon Lord liked food too, not as much as Bile but enough so to rob everyone of their food, especially Bile.\nHungry and ready for action Bile will join with you.", 500, 6);

    /**
     * The character Maata Rangi
     * @author Dwight Collier
     */
    public static Character maata = new Character("Maata Rangi", "Maata is a fun loving girl from New Zealand.\nMaata was a bit mischievous but her intentions are good and well.\nShe heard about the demon lord and the tragedies he caused.\nMaata knew that there was only room for one mischievous person.\nMaata will fight in the name of good mischief.", 200, 8);

    /**
     * The character Noa Kimiko
     * @author Dwight Collier
     */
    public static Character noa = new Character("Noa Kimiko", "Meet Noa, or should I say Kimiko, in true Japanese fashion she prefers to be called by her last name.\nKimiko loves her home, Japan.\nShe has so much pride in her country.\nWhen someone insults Japan, her pride she takes GREAT offense.\nShe will break an arm or two, and on a good day it will only be your arms.\nNow you may have sensed a demon lord trend by now so long story short the Demon Lord ruined Japan.\nKimiko wasn’t afraid but infuriated.\nKimiko will happily join you, as long as she gets the chance to kill the Demon Lord.", 350, 8);

    /**
     * The character Zeno Mattia
     * @author Dwight Collier
     */
    public static Character zeno = new Character("Zeno Mattia", "Zeno is from Italy. He is a fan of the most expensive and luxurious things.\nHe wasn’t greedy but he loved to go out of his way to look at beautiful gems, fancy rings etc.\nHe was a very materialistic person who could only find happiness through fancy items.\nHis happiness was cut short when the demon lord took everything luxurious from him, he was broken.\nZeno will face the Demon Lord for his items.", 400, 5);

    /**
     * The character Pika Leilana
     * @author Dwight Collier
     */
    public static Character pika = new Character("Pika Leilana", "Pika considers himself to be the best fighter out there.\nHe beat everyone in his village and felt unstoppable, so when he heard about the Demon Lord he felt that the demon lord would be a ‘decent’ challenge.\nHe may be a lot of talk but he has some bite; he can’t 1v1 the Demon Lord but he could certainly fight him with a team.", 300, 8);

    /**
     * The character Cheftzi Marav
     * @author Dwight Collier
     */
    public static Character cheftzi = new Character("Cheftzi Marav", "Cheftzi was the chief of her tribe, they were fearsome warriors, excellent hunters, and natural killers.\nBut above all they were a family.\nCheftzi's tribe meant everything to her. She was the leader, responsible for their safety.\nThe Demon Lord however demolished her tribe without a second thought, her family was gone and all that was left was vengeance.\nHer tribe can’t be protected anymore, only avenged, now she will show you what a natural killer can do.", 150, 15);

    /**
     * The character Bludsplaz
     * @author Dwight Collier
     */
    public static Character bludsplaz = new Character("Bludsplaz", "Bludsplaz is a vampire, and as a vampire he feels that he is above all humans.\n\"I should rule them all,\" he would always think to himself.\nHowever the Demon Lord Apocalypse is an obstacle in the way of his domination.\nHe knows he couldn’t beat him alone, \"Maybe I’ll go with strong human allies.\"\nThat is when he heard about you, he now wishes to accompany you, to defeat the Demon Lord. While also resisting the urge to drink your blood.\nHe'll be fine, just fine...", 300, 8);

    /**
     * The character Dapa Rusher
     * @author Dwight Collier
     */
    public static Character dapa = new Character("Dapa Rusher", "Dapa is a werewolf.\nAs a werewolf he wouldn’t eat people but he would go out of his way to terrify them.\nDapa always had a great time frightening them, oh the look on their faces!!\nOne day they weren’t afraid anymore, and he asked them \"why aren’t you scared?\"\nA little girl of all people was the first to respond, \"Because the Demon Lord is way scarier!\"\nA Demon Lord, scarier than a werewolf?\nImpossible Dapa thought, but if that was true then he wouldn’t be able to scare the humans anymore.\nThe only possible solution is to prove that he is the scarier monster!! Dapa will join you and bring terror to your enemies.", 500, 6);

    /**
     * The character Oschuan Marshall
     * @author Dwight Collier
     */
    public static Character oschuan = new Character("Oschuan Marshall", "Oschuan Marshall was an AP Comp teacher.\nPreferring to be called Mr. Marshall, he would teach his class until one fateful day when the demon lord would approach him.\n\"I have an interest in coding\" the demon lord said.\nMr. Marshall was eager to teach any kind of student, even the world conquering ones.\nHe gave the Demon Lord an assignment due by midnight that night.\nNot only did the demon lord turn it in one minute late, his program wouldn’t compile.\nHe also found evidence of plagiarism, so Mr. Marshall had no choice but to give him that zero.\nEnraged, the Demon Lord destroyed all of Mr. Marshall'ss cprograms.\nAfterwards the only logical action left to Mr. Marshall was to eliminate the threat to coding.", 460, 10);

    /**
     * The character Vesper Amarantha
     * @author Dwight Collier
     */
    public static Character vesper = new Character("Vesper Amarantha", "Vesper was so emo, and so attached to social media.\nShe was like, totally goth too.\nAnd like Vesper learned about this totally uncool Demon Lord and like she couldn’t ignore it you know?\nVesper is the only one allowed to make the people around her moody, so now it was time for like a total beatdown.", 200, 5);

    /**
     * The character Paco Valero
     * @author Dwight Collier
     */
    public static Character paco = new Character("Paco Valero", "Paco was having a good time in Spain.\nPaco was eating salsa with Juan.\nThe Demon Lord saw that delicious salsa and asked for some.\nBeing the greedy b*stard that Paco is, he didn’t share.\nSo the Demon Lord killed Juan.\nWhy Juan and not Paco?\nEhhh i don’t know holmes, but what I do know is that Paco is now out for vengeance, \"Para mi amigo!!\"", 370, 8);

	
	/**
	 * Static constructor. Just adds the items to the characters.
     * @author Dwight Collier, Eric Schneider
	 */
	static {
		albina.addItem(goldSword);
		albina.addItem(silverCoin, 5);
		
        grigorii.addItem(goldSword);
        grigorii.addItem(silverCoin, 5);
		
        dipti.addItem(bat);
        dipti.addItem(silverCoin, 5);
		
        scourge.addItem(silverSword);
        scourge.addItem(silverCoin, 5);
		
        dope.addItem(silverSword);
        dope.addItem(silverCoin, 5);
		
        fritzi.addItem(silverSword);
        fritzi.addItem(silverCoin, 5);
		
        judoc.addItem(estoc);
        judoc.addItem(silverCoin, 5);
		
        bile.addItem(bronzeSword);
        bile.addItem(silverCoin, 5);
        
        maata.addItem(bronzeSword);
        maata.addItem(silverCoin, 5);
        
        noa.addItem(ironSword);
        noa.addItem(silverCoin, 5);
        
        zeno.addItem(ironSword);
        zeno.addItem(silverCoin, 5);
        
        pika.addItem(ironSword);
        pika.addItem(silverCoin, 5);
        
        cheftzi.addItem(steelSword);
        cheftzi.addItem(silverCoin, 5);
        
        bludsplaz.addItem(steelSword);
        bludsplaz.addItem(silverCoin, 5);
        
        dapa.addItem(claw);
        dapa.addItem(silverCoin, 5);
        
        oschuan.addItem(dp);
        oschuan.addItem(silverCoin, 5);
        
        vesper.addItem(diamondSword);
        vesper.addItem(silverCoin, 5);
        
        paco.addItem(diamondSword);
        paco.addItem(silverCoin, 5);
	}
}
