package teameric.zeldomistrust.entities;

import teameric.zeldomistrust.items.Items;

/**
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider, Dwight Collier
 */
public class Monsters {

    /**
     * The Orc monster.
     * @author Eric Schneider
     */
    public static Entity orc = new Entity("Orc", "Green and ugly.", 20, 3);

    /**
     * The Bandit monster.
     * @author Dwight Collier, Eric Schneider
     */
    public static Entity bandit = new Entity("Bandit", "Cunning and quick, they kill for fun.", 56, 5);

    /**
     * The Orc Guard monster.
     * @author Eric Schneider
     */
    public static Entity orcGuard = new Entity("Orc Guard", "Green and ugly. Watch out, these guys are well-trained.", 50, 4);

    /**
     * The Lizard Man monster.
     * @author Dwight Collier, Eric Schneider
     */
    public static Entity lizardMan = new Entity("Lizard Man", "A Lizard that is human like in feature, the creatures of brutality.", 110, 6);

    /**
     * The Orc King monster.
     * @author Eric Schneider
     */
    public static Entity orcKing = new Entity("Orc King", "This guy is big and scary. The Orc King rules all of the Orcs, or well, all the Orcs that listen to him anyway, which isn't really that many. Still scary though.", 200, 5);

    /**
     * The Lizard Captain monster.
     * @author Dwight Collier, Eric Schneider
     */
    public static Entity lizardCaptain = new Entity("Lizard Captain", "This lizard man is a captain and the captains are known to be cruel, be careful around them.", 500, 5);

    /**
     * The Possesed Pika monster.
     * @author Dwight Collier, Eric Schneider
     */
    public static Entity possesedPika = new Entity("Possesed Pika", "Pika is under some sort of spell, he's stronger than he normally is, be careful.",400, 4);

    /**
     * The Dark Knight monster.
     * @author Dwight Collier, Eric Schneider
     */
    public static Entity darkKnight = new Entity("Dark Knight", "Loyal to the demon lord, these dark knights don't hit hard but they can take punishment.", 300, 2);

    /**
     * The Christian Collier monster.
     * @author Dwight Collier, Eric Schneider
     */
    public static Entity christianCollier = new Entity("Christian Collier", "One of the developers of ZeldoMistrust, he should be a worthy foe, even if he may or may not be hacking.", 750, 5);
	
    /**
     * The Demon monster.
     * @author Eric Schneider
     */
    public static Entity demon = new Entity("Demon", "A Demon from Hell.", 30, 5);

    /**
     * The Demon King monster.
     * @author Eric Schneider
     */
    public static Entity demonKing = new Entity("Demon King", "The Demon King himself. If you beat him, you win!", 1000, 10);
	
	/**
	 * Static constructor. Current just adds the items to the monsters.
	 * @author Eric Schneider, Dwight Collier
	 */
	static {
		orc.addItem(Items.bat);
		orc.addItem(Items.bronzeCoin, 3);
		
		lizardMan.addItem(Items.ironSword);
		lizardMan.addItem(Items.bronzeCoin, 5);
		
		darkKnight.addItem(Items.ironSword);
		darkKnight.addItem(Items.bronzeCoin, 5);
		
		lizardCaptain.addItem(Items.ironSword);
		lizardCaptain.addItem(Items.bronzeCoin, 5);
		
		orcGuard.addItem(Items.silverSword);
		orcGuard.addItem(Items.bronzeCoin, 5);
		
		bandit.addItem(Items.silverSword);
		bandit.addItem(Items.bronzeCoin, 5);
		
		orcKing.addItem(Items.silverSword);
		orcKing.addItem(Items.silverCoin, 5);
		
		possesedPika.addItem(Items.ironSword);
		possesedPika.addItem(Items.bronzeCoin, 5);
		
		christianCollier.addItem(Items.steelSword);
		christianCollier.addItem(Items.goldCoin, 5);
		
		demonKing.addItem(Items.diamondSword);
		demonKing.addItem(Items.goldCoin, 20);
	}
}
