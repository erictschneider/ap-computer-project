package teameric.zeldomistrust.entities;

import teameric.zeldomistrust.items.Item;
import java.util.HashMap;
import teameric.zeldomistrust.items.Sword;

/**
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider, Zachary Stanelle
 */
public class Entity {

	/**
	 * Name of the entity
	 * @author Eric Schneider, Zachary Stanelle
	 */
	protected String name;

	/**
	 * Lore of the entity
	 * @author Eric Schneider, Zachary Stanelle
	 */
	protected String lore;

	/**
	 * Health of the entity
	 * @author Eric Schneider, Zachary Stanelle
	 * 
	 */
	protected int health;

	/**
	 * Max health for any entity
	 * @author Eric Schneider, Zachary Stanelle
	 * 
	 */
	protected int maxHealth;

	/**
	 * Damage for an entity
	 * @author Eric Schneider, Zachary Stanelle
	 * 
	 */
	protected int damage;

	/**
	 * The index is the item and the value is the number of that item it contains.
	 * @author Eric Schneider, Zachary Stanelle
	 * 
	 */
	protected HashMap<Item, Integer> items;
	/**
	 * Contains the sword used by the entity (see Items.java for list)
	 * @author Eric Schneider, Zachary Stanelle
	 */
	protected Sword sword;
	
	/**
	 *
	 * @param name This will set the entity name to the name received from the name method. 
	 * @param lore This is will set the lore of the entity received from the method entity earlier
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public Entity(String name, String lore){
		this(name, lore, 100, 25, null);
	}
	
	/**
	 * Constructs the object
	 * @param name used by constructor for the name of the object
	 * @param lore used by the constructor to set the lore of the object
	 * @param maxHealth used by the constructor to set the max health that an object will have
	 * @param damage used by the constructor the set the amount of damage this object will inflict
	 *@author Eric Schneider, Zachary Stanelle
	 */
	public Entity(String name, String lore, int maxHealth, int damage){
		this(name, lore, maxHealth, damage, null);
	}
	
	/**
	 *
	 * @param name sets the name variable in this method
	 * @param lore sets the lore variable in this method
	 * @param maxHealth sets the maxHealth variable in this method
	 * @param damage sets the damage variable in this method
	 * @param items inserts the HashMap into the method.
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public Entity(String name, String lore, int maxHealth, int damage, HashMap<Item, Integer> items){
		this.name = name;
		this.lore = lore;
		this.health = maxHealth;
		this.maxHealth = maxHealth;
		this.damage = damage;
		
		if(items != null){
			this.items = items;
			
			items.forEach((item, num) -> {
				for(int i = 1; i <= num; i++)
					item.applyToEntity(this);
			});
		}else
			this.items = new HashMap<>();
	}
	
	/**
	 * Get name method
	 * @return the name of the Entity
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * This method gets the lore
	 * @return the lore is being returned here
     * @author Eric Schneider, Zachary Stanelle
	 */
	public String getLore(){
		return lore;
	}
	
	/**
	 * This method gets the health variable
	 * @return the health variable
     * @author Eric Schneider, Zachary Stanelle
	 */
	public int getHealth(){
		return health;
	}
	
	/**
	 * Getter for the entity's max health.
	 * @return the entity's max health
     * @author Eric Schneider, Zachary Stanelle
	 */
	public int getMaxHealth(){
		return maxHealth;
	}
	
	/**
	 * Getter for the entity's damage
	 * @return the entity's damage
     * @author Eric Schneider, Zachary Stanelle
	 */
	public int getDamage(){
		return damage;
	}
	
	/**
	 *
	 * @return the damage done
	 * this gets the attack amount done.
	 */
	public int getAttack(){
		if(sword != null)
			return damage + sword.getDamage();
		return damage;
	}
	
	/**
	 *
	 * @return of the sword variabele
	 * gets the sword type
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public Sword getSword(){
		return sword;
	}
	
	/**
	 *
	 * @return the itemes variable
	 * This gets the hashmap of the for the items and their values.
	 *@author Eric Schneider, Zachary Stanelle
	 */
	public HashMap<Item, Integer> getItems(){
		return items;
	}
	
	/**
	 * Complimentary variable to getValue(). Used since lambdas act as anonymous classes and cannot access local variables.
     * @author Eric Schneider, Zachary Stanelle
	 */
	private int getValRet;
	
	/**
	 *
	 * @return of the value of the item
	 * the value for the item is returned
	 *@author Eric Schneider, Zachary Stanelle
	 */
	public int getValue(){
		getValRet = 0;
		
		items.forEach((item, num) -> {
			for(int i = 1; i <= num; i++)
				getValRet += item.getValue();
		});
		
		return getValRet;
	}
	
	/**
	 *
	 * @param item This sends in the item variable
	 * this will add an item for your charecter
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public void addItem(Item item){
		addItem(item, 1);
	}
	
	/**
	 *
	 * @param item sending in the item variable
	 * @param num sending in the num variabale
	 * Adds an item to the entity
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public void addItem(Item item, int num){
		if(items.get(item) != null)
			items.put(item, items.get(item) + num);
		else
			items.put(item, num);
		
		for(int i = 1; i <= num; i++)
			item.applyToEntity(this);
	}
	
	/**
	 * Sets the name of the character
	 * @param name the name of the character
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 *
	 * @param lore passes in lore
	 * sets lore
	 *@author Eric Schneider, Zachary Stanelle
	 */
	public void setLore(String lore){
		this.lore = lore;
	}
	
	/**
	 *
	 * @param health passes in health
	 * sets health
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public void setHealth(int health){
		this.health = health;
	}
	
	/**
	 *
	 * @param damage passes damage variable
	 * sets damage
	 * @author Eric Schneider, Zachary Stanelle
	 */
	public void setDamage(int damage){
		this.damage = damage;
	}
	
	/**
	 *
	 * @param sword passes sword
	 * sets sword
	 */
	public void setSword(Sword sword){
		this.sword = sword;
	}
	
	/**
	 * Gives a string representation of the object (just the name).
	 * @return string representation of the object
	 * @author Eric Schneider
	 */
	@Override
	public String toString(){
		return name;
	}
	
	/**
	 * Constructs a new entity based off an already existing one.
	 * @return a copy of the entity.
	 * @author Eric Schneider
	 */
	public Entity copy(){
		return new Entity(name, lore, maxHealth, damage, items);
	}
	
	/**
	 *
	 * @param entity enittiy varible passed in
	 * @return return true or false
	 * test if entity name is equal
	 */
	public boolean equals(Entity entity){
		return entity.getName().equals(getName());
	}
	
	/**
	 *
	 * @param str str variable passed in
	 * @return return true or false
	 * test if str is equal
	 */
	public boolean equals(String str){
		return str.equals(getName().toLowerCase());
	}
}
