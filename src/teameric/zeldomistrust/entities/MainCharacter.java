package teameric.zeldomistrust.entities;

import java.util.ArrayList;
import java.util.HashMap;

import teameric.zeldomistrust.items.Item;

/**
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider
 */
public class MainCharacter extends Character {

    /**
     * Contains all of the main character's characters.
     * @author Eric Schneider
     */
    protected ArrayList<Character> cast;
	
    /**
     * Constructs a new main character/player
     * @param name the name of the player
     * @param lore the lore
     * @param maxHealth the player's max health
     * @param damage the player's damage
     * @param items the player's inventory
     */
    public MainCharacter(String name, String lore, int maxHealth, int damage, HashMap<Item, Integer> items){
		super(name, lore, maxHealth, damage, items);
		cast = new ArrayList<>();
	}
	
    /**
     * Getter method for cast object
     * @return the player's cast
     */
    public ArrayList<Character> getCast(){
		return cast;
	}
	
    /**
     * Adds a Character to the cast
     * @param c the Character
     */
    public void addToCast(Character c){
		cast.add(c);
	}
	
	@Override
	public boolean isPlayer(){
		return true;
	}
}
