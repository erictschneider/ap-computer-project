package teameric.zeldomistrust.entities;

import java.util.HashMap;
import teameric.zeldomistrust.items.Item;

/**
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider, Dwight Collier
 *
 */
public class Character extends Entity {

    /**
     * The loyalty of the character
     * @author Eric Schneider
     */
    protected int loyalty = 100;
	
    /**
     * This will set the default loyalty
     * @param name, the name of the Character
     * @param lore, the Character's extensive lore
     * 
     */
    public Character(String name, String lore){
		this(name, lore, 100, 0, null);
	}
	
    /**
     * sets the name, lore, health, and damage a character can do.
     * @param name, the name of the character
     * @param lore, sets the lore for the character
     * @param maxHealth, sets the maxHealth a character can have
     * @param damage, sets the max damage a character can do
     */
    public Character(String name, String lore, int maxHealth, int damage){
		this(name, lore, maxHealth, damage, null);
	}
	
    /**
     *
     * @param name, the name of the character
     * @param lore, sets the lore for the character
     * @param maxHealth, sets the maxHealth a character can have
     * @param damage, sets the max damage a character can do
     * @param items, sets the default items for the character
     */
    public Character(String name, String lore, int maxHealth, int damage, HashMap<Item, Integer> items){
		super(name, lore, maxHealth, damage, items);
	}
    
    /**
     * Getter method for loyalty
     * @return the loyalty
     */
    public int getLoyalty(){
        return loyalty;
    }
    
    /**
     *
     * @param loyalty, gets the loyalty for the character
     */
    public void setLoyalty(int loyalty){
        this.loyalty = loyalty;
    }
	
    /**
     * @return true if this is the main player, false otherwise
     */
    public boolean isPlayer(){
		return false;
   /**
     *
     * @param loyalty
     * @param isPlayer
     * @return
     */
        
	}
}
