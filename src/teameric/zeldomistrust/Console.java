package teameric.zeldomistrust;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * This class contains a bunch of utility methods for Console input and output.
 * 
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider
 */
public class Console {
    /**
     * Private variable used for input.
     * @author Eric Schneider
     */
	private static Scanner scanner = new Scanner(System.in);
	
    /**
     * Equivalent to System.out.print
     * @param output the String to be output.
     * @author Eric Schneider
     */
    public static void print(String output){
		System.out.print(output);
	}
	
    /**
     * Equivalent to System.out.println.
     * @author Eric Schneider
     */
    public static void line(){
		System.out.println();
	}
	
    /**
     * Equivalent to System.out.print.
     * @param output the String to be output
     * @author Eric Schneider
     */
    public static void line(String output){
		System.out.println(output);
	}
	
    /**
     * Gets a number based on a min and max.
     * @param prompt the prompt displayed to the user
     * @param min the minimum value
     * @param max the maximum value
     * @return the int gotten
     * @author Eric Schneider
     */
    public static int getInt(String prompt, int min, int max){
		line(prompt);
		print(":");
		int ret = scanner.nextInt();
		
		if(ret < min || ret > max){
			line("Your input is out of the range of [" + min + ", " + max + "].");
			line("Please enter another number!");
			line();
			return getInt(prompt, min, max);
		}
		
		return ret;
	}
	
    /**
     * Gets a String
     * @param prompt the prompt displayed to the user
     * @return the String gotten
     * @author Eric Schneider
     */
    public static String getString(String prompt){
		line(prompt);
		print(":");
		return scanner.nextLine();
	}
	
    /**
     * Gets a String
     * @param prompt the prompt displayed to the user
     * @param possibleInputs the possible inputs that the user can input
     * @return the String gotten
     * @author Eric Schneider
     */
    public static String getString(String prompt, String[] possibleInputs){
		return getString(prompt, new ArrayList<>(Arrays.asList(possibleInputs)));
	}
	
    /**
     * Gets a String
     * @param prompt the prompt displayed to the user
     * @param possibleInputs the possible inputs that the user can input
     * @param hiddenInputs the possible inputs that won't be told (used for cheating)
     * @return the String gotten
     * @author Eric Schneider
     */
    public static String getString(String prompt, String[] possibleInputs, String[] hiddenInputs){
		return getString(prompt, new ArrayList<>(Arrays.asList(possibleInputs)), new ArrayList<>(Arrays.asList(hiddenInputs)));
	}
	
    /**
     * Gets a String
     * @param prompt the prompt displayed to the user
     * @param possibleInputs the possible inputs that the user can input
     * @return the String gotten
     * @author Eric Schneider
     */
    public static String getString(String prompt, ArrayList<String> possibleInputs){
		return getString(prompt, possibleInputs, new ArrayList<>());
	}
	
    /**
     * Gets a String
     * @param prompt the prompt displayed to the user
     * @param possibleInputs the possible inputs that the user can input
     * @param hiddenInputs the possible inputs that won't be told (used for cheating)
     * @return the String gotten
     * @author Eric Schneider
     */
    public static String getString(String prompt, ArrayList<String> possibleInputs, ArrayList<String> hiddenInputs){
		line(prompt);
		line("Possible inputs include: " + possibleInputs);
		print(":");
		
		String ret = scanner.nextLine();
		
		if(!(possibleInputs.contains(ret) || hiddenInputs.contains(ret))){
			line("Your input is not an acceptable input.");
			line("Please enter another string!");
			line();
			return getString(prompt, possibleInputs);
		}
		
		return ret;
	}
	
    /**
     * Gets a boolean yes or no.
     * @param prompt the prompt displayed to the user
     * @return the String gotten
     * @author Eric Schneider
     */
    public static boolean confirm(String prompt){
		line(prompt);
		print(":");
		String input = scanner.nextLine();
		
		ArrayList<String> yeses = new ArrayList<>(Arrays.asList(new String[]{"yes", "y", "Y", "sure"}));
		ArrayList<String> nos = new ArrayList<>(Arrays.asList(new String[]{"no", "n", "N", "nah"}));
		
		if(yeses.contains(input))
			return true;
		else if(nos.contains(input))
			return false;
		else{
			line("Your input is not an acceptable input.");
			line("Please enter another string!");
			line();
			return confirm(prompt);
		}
	}
}
