package teameric.zeldomistrust;

import static teameric.zeldomistrust.ZeldoMistrust.*;
import static teameric.zeldomistrust.Console.*;
import teameric.zeldomistrust.entities.Characters;
import teameric.zeldomistrust.entities.Monsters;

/**
 * The Alpha Dungeon, the first dungeon.
 * 
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider
 */
public class AlphaDungeon {

    /**
     * @author Eric Schneider
     * @throws java.lang.InterruptedException should not throw
     */
    public static void play() throws InterruptedException {
        line("Welcome!");
        line("To the world of Zeldo Mistrust...");
        Thread.sleep(4000); // 4 second pause
        line();
        line("\"Or something,\" snickers a local peasant.");
        line("\"This game sounds pretty stupid to me... I mean, the name is literally a rip-off of some other game that's nothing like this game.\"");
        Thread.sleep(6000); // 6 second pause
        line();
        line("A woman walks up, and speaks to you.");
        line("\"Ignore that stupid fool... I'm Albina, and I need your help.\"");
        line("Albina gives you her story...");
        line();
        Thread.sleep(8000); // 8 second pause
        line(Characters.albina.getLore());
        Thread.sleep(15_000); // 15 second pause
        line();
        
        if(confirm("\"Can I join you on my quest?\"")){
            main.addToCast(Characters.albina);
            line("\"Ya! Ya! I am very gracias! For Russia!\"");
        }else
            line("\"Very well... I understand that you're worried I might drag you down.\"");

        Thread.sleep(5000); // 5 second pause
        line();
        line("The snarky peasant is approaching you again...");
        line("\"The narrator told me to tell you... er, I mean, uh, gotta stay in character...");
        line("My name is Andrew, and I live in this local village. Andrewville (kek, why not)");
        line("We have been attacked by monsters... a band of local Orcs, as a matter of fact.");
        Thread.sleep(10_000); // 10 second pause
        line("Holy quacamola, there's one right over there!\"");
        Thread.sleep(2000); // 2 second pause
        fight(Monsters.orc.copy());
        line("\"Well, good job. You braved your first battle.");
        line("Let me give you some advice on battles. As you adventure through this world, you build up a cast.");
        line("You'll want your cast to fight the battles instead of you.");
        line("However, you should be careful, because if you use one character too many times their loyalty will fall.");
        line("They might even attack you. Of course, by that point, it should be easy to fend them off.\"");
        Thread.sleep(15_000); // 15 second pause
        line();
        line("A wind blows.");
        line("\"There's more where that comes from,\" explains the peasant");
        line("\"That Orc is part of a group of Orcs, all lead by the Orc King. Bad guy.");
        line("The Orc King isn't really the king of all Orcs, he's more like a crazy cult leader who wishes he was.");
        line("Don't tell him that though... Let me just say though, he doesn't represent all Orcs at all, not even a little bit.");
        line("In fact, I'm friends with some Orcs who don't like him at all, and are perfectly fine people. Uh, anyway, moving on...\"");
        Thread.sleep(15_000); // 15 second pause
        line();
        line("\"Your next stop is China. China is right next to Russia, so uh, it'll be a short trip.\"");
        Thread.sleep(5000); // 5 second pause
        line();
        line("\"Welcome to China!\" yells a young man. He introduces himself as Grigorii.");
        line("Grigorii gives you his story...");
        Thread.sleep(5000); // 5 second pause
        line();
        line(Characters.grigorii.getLore());
        Thread.sleep(15_000); // 15 second pause
        line();
        
        if(confirm("\"Can I join you on my quest?\"")){
            main.addToCast(Characters.grigorii);
            line("\"It is a honor for me to serve.\"");
        }else{
            line("\"Fine then! You're disgusting, just like the rest of them. You're probably another agent of them.");
            line("\"Let me show you what I do to demon-supporting scum like you...\"");
            Thread.sleep(5000); // 5 second pause
            fight(Characters.grigorii);
        }

        Thread.sleep(5000); // 5 second pause
        line();
        line("A peasant approaches. \"Sorry about Grigorii, he's just a little bit emotional.\"");
        line("The peasant points to a cave. \"This where they've been hiding out.");
        line("This is your chance to stop evil, once and for all. You must defeat that Demon-affiliated Orc King, a total cultist b****.\"");
        Thread.sleep(8000); // 8 second pause

        if(confirm("Will you enter the cave?")){
            line("You enter the cave...");
            line("The Orc King left, although some of his minions are still around. Maybe this was a waste of time.");
            line();
            Thread.sleep(5000); // 5 second pause
            fight(Monsters.orc.copy());
            fight(Monsters.orc.copy());
            fight(Monsters.orcGuard.copy());
            line();
            line("\"Hold up, you crazy tree!\" yells an Indian man. You greet him with much akwardness.");
            line("\"I am on that good sh*t, tree!\"");
            line("It's hard to explain that you aren't a tree to this odd, hallucinating man. You hope he doesn't try to smoke you.");
            line("You are tree. I am Dipti. Let me tell your my story...");
            Thread.sleep(8000); // 8 second pause
            line(Characters.dipti.getLore());
            Thread.sleep(15_000); // 15 second pause
            
            if(confirm("\"Can I join you trees on my quest?\"")){
                main.addToCast(Characters.dipti);
                line("\"Yipitee! I can't wait to smoke you... I mean, defeat the demon king. I'll get sober by tomorrow, I promise!\"");
            }else{
                line("\"Fine fine. Could you at least give me some money to buy, uh, a vase, yes yes, a vase.\"");
                line("You politely decline and move on.");
            }
            
            Thread.sleep(5000); // 5 second pause
            line();
            line("\"Hey, man!\" yells another man. \"You saved me!");
            line("Sorry about Dipti, he's got... problems. Anyway, I want to join you.\"");
            line("The man tells his story...");
            Thread.sleep(6000); // 6 second pause
            line(Characters.scourge.getLore());
            Thread.sleep(15_000); // 15 second pause
            
            if(confirm("\"Can I join you to defeat the Demon Lord?\"")){
                main.addToCast(Characters.scourge);
                line("\"Alright, let's do this.\"");
            }else
                line("\"Fine then! Screw you, man! I'll defeat the Demon Lord by myself and then move on.\"");
            
            Thread.sleep(5000); // 5 second pause
            line("You leave the cave.");
            Thread.sleep(2000); // 2 second pause
        }else if(main.getCast().contains(Characters.grigorii)){
            line("Grigorii yells. \"Okay, man!");
            line("These are just my people, man! Not cool.\"");
            Characters.grigorii.setLoyalty(50);
        }
        
        line();
        line("The peasant approaches you. \"Okay, they left that cave, and now they're in that cave.\"");
        
        if(main.getCast().contains(Characters.grigorii))
            line("Grigorii grumbles. \"What a waste of time!\"");
        
        Thread.sleep(5000); // 5 second pause
        
        line();
        line("You enter the cave, and are greeted by three Orcs.");
        fight(Monsters.orc.copy());
        fight(Monsters.orc.copy());
        fight(Monsters.orc.copy());
        Thread.sleep(2000); // 2 second pause
        
        if(main.getCast().contains(Characters.dipti)){
            line();
            line("\"Oh god...\" starts Dipti. \"I think the sh*t's wearing off...");
            line("I guess you aren't a yummy tree, but those green Orcs... yum...");
            Thread.sleep(4000); // 4 second pause
        }
        
        line("You move further into the cave.");
        Thread.sleep(2000); // 2 second pause
        line("And there is the Orc King... he wears a crown that looks like trash glued together and painted yellow.");
        line("The Orc King is surrounded by four Orc guards, all who look more competent than he does.");
        Thread.sleep(5000); // 5 second pause
        
        if(confirm("Do you want to take them on?")){
            line("You and your cast form a circle and chant inspiring words.");
            
            if(main.getCast().contains(Characters.albina))
                line("\"Ya, ya! For Russia!!!\" yells Albina. She slurps down a vodka milkshake.");
            
            if(main.getCast().contains(Characters.grigorii))
                line("\"Alright, time to kill this demon-butt-killer!\" screams Grigorii. He is... hyped, for lack of a better word.");
            
            if(main.getCast().contains(Characters.dipti)){
                line("Dipti isn't really sure where he is anymore. Turns out whatever he took last night wasn't really wearing out after all.");
                line("The excitement prompts Dipti to compliment everyone's colorful leaves.");
            }
            
            if(main.getCast().contains(Characters.scourge))
                line("\"Okay, let's do this boys,\" says Scourge. He's a bit nervous, but otherwise he's fully mentally prepared.");
            
            Thread.sleep(3000 + (main.getCast().size() * 3000)); // See what I did there? #masterprogrammer
        }else{
            line("You hear footsteps coming from behind you. You turn around, and it's that darn sarcastic peasant!");
            line("\"You know you don't really have a choice in the matter, right? This isn't that kind of game.\"");
            line("You grumble as he leaves, and then you mobilize your squard.");
            Thread.sleep(8000); // 8 second pause
        }
        
        line("It's time to fight!");
        Thread.sleep(2000); // 2 second pause
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        Thread.sleep(2000); // 2 second pause
        line();
        line("The Orc King is shaken by the defeat of his comrads.");
        line("He calls you all sorts of bad names, but luckily, he doesn't speak much English, so you aren't really demotivated by any of them.");
        Thread.sleep(6000); // 6 second pause
        fight(Monsters.orcKing.copy());
        Thread.sleep(2000); // 2 second pause
        line("You defeated the Orc King!");
        Thread.sleep(3000); // 3 second pause
        line("\"Hey, man! Get me out of this can!\" yells a young man locked up in a cage.");
        line("You find the key and unlocked him. He tells his story...");
        Thread.sleep(6000); // 6 second pause
        line(Characters.dope.getLore());
        Thread.sleep(15_000); // 15 second pause
            
        if(confirm("\"Can I join you on your quest? I swear man, I'll give it my best!\"")){
            main.addToCast(Characters.dope);
            line("\"Yeah, yeah, yeah... we gonna kill this demon... it'll be gone, TEAM IN!\"");
        }else
            line("\"Fine man, I'll do this by myself! Screw you, you short elf!\"");
        
        Thread.sleep(4000); // 4 second pause
        line("You leave the cave. The peasant gives you a map.");
        line("\"Okay so, from here, now you have to go to there. To what we call the \"Beta Dungeon.\"");
        line("Didn't have anything better to call it, I guess.\"");
        line("You move on....");
        Thread.sleep(6000); // 6 second pause
    }
}
