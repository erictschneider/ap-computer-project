package teameric.zeldomistrust;

import teameric.zeldomistrust.entities.MainCharacter;
import teameric.zeldomistrust.entities.Entity;
import teameric.zeldomistrust.entities.Character;
import java.util.ArrayList;
import java.util.HashMap;
import static teameric.zeldomistrust.Console.*;
import teameric.zeldomistrust.items.Item;
import teameric.zeldomistrust.items.Items;

/**
 * This is the main class.
 * 
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider
 */
public class ZeldoMistrust {

	/**
	 * The main character. Only one in the game. If dead, the game should be over.
	 * 
	 * @author Eric Schneider
	 */
	public static MainCharacter main;
	
    /**
	 * The main method.
	 * 
     * @param args the command line arguments
	 * @author Eric Schneider
     */
    public static void main(String[] args){
        try {
            main = createMainCharacter();
            AlphaDungeon.play();
			BetaDungeon.play();
			OmegaDungeon.play();
        }catch (InterruptedException e){ // This game is all one thread, so there's absolutely no reason this should happen.
            e.printStackTrace();
        }
    }
	
	/**
	 * Takes care of the main character selection, letting the player choose among a few different classes.
	 * @return the constructed main character
	 * @author Eric Schneider
     * @throws java.lang.InterruptedException should not throw
	 */
	public static MainCharacter createMainCharacter() throws InterruptedException {
		String name = getString("Character, what is your name?");
		line();
		line("Hello " + name + "!");
        Thread.sleep(2000); // 2 second break
		
		int maxHealth = 0;
        int damage = 0;
		HashMap<Item, Integer> items = new HashMap<>();
		
		switch(getString("Now is your time to choose among a few classes. Which shall you choose?", new String[]{"warrior", "paladin", "scout", "trickster", "rogue"}, new String[]{"eric"})){
			case "warrior":
				maxHealth = 1000;
                damage = 10;
				items.put(Items.bronzeSword, 1);
				items.put(Items.goldCoin, 5);
				break;
            case "paladin":
                maxHealth = 1000;
                damage = 10;
                items.put(Items.ironSword, 1);
				items.put(Items.goldCoin, 5);
                break;
            case "scout":
                maxHealth = 500;
                damage = 20;
                items.put(Items.bat, 1);
				items.put(Items.goldCoin, 10);
                break;
            case "trickster":
                maxHealth = 500;
                damage = 15;
                items.put(Items.steelSword, 1);
				items.put(Items.goldCoin, 10);
				break;
            case "rogue":
                maxHealth = 1500;
                damage = 10;
                items.put(Items.silverSword, 1);
				items.put(Items.goldCoin, 5);
				items.put(Items.silverCoin, 5);
				break;
			case "eric":
				maxHealth = 100_000;
                damage = 1000;
				items.put(Items.ericSword, 1);
				items.put(Items.goldCoin, 100);
				break;
		}
		
		return new MainCharacter(name, "It's you!", maxHealth, damage, items);
	}
	
	/**
	 * This method is used to choose the character when in a battle (this is used in fight()).
	 * @return the chosen character, or the main character.
	 * @author Eric Schneider
     * @throws java.lang.InterruptedException should not throw
	 */
	public static Character chooseCharacter() throws InterruptedException {
		Character ret = null;
		
		if(main.getCast().isEmpty()){
			ret = main;
			line("You don't have any friends, so you're just going to have to do the dirty work yourself.");
			
			Thread.sleep(2000); // 2 second break
		}else{
			ArrayList<String> possibleCharacters = new ArrayList<>();
			
			main.getCast().forEach(c -> possibleCharacters.add(c.toString().toLowerCase()));
			
			String choosen = getString("Which character shall you choose?", possibleCharacters);
			
			for(Character c : main.getCast())
				if(c.equals(choosen))
					ret = c;
			try {
				ret.setLoyalty(ret.getLoyalty() - 3);
				line();
				line("Sending out " + ret + "!");
			}catch(NullPointerException e){ // This shouldn't happen but just in case.
				line(e.getMessage());
				ret = main;
			}
		}
		
		return ret;
	}
	
	/**
	 * Used for a fight. This is only 1v1, sadly. The character is used is called with chooseCharacter().
	 * @param enemy the enemy to be fought.
	 * @author Eric Schneider
     * @throws java.lang.InterruptedException should not throw
	 */
	public static void fight(Entity enemy) throws InterruptedException {
		line();
		print(main + ", it's time to fight " + enemy + "!");
        
        if(!(enemy instanceof Character))
            line(" (" + enemy.getLore() + ")");
        else
            line();
        
        Thread.sleep(2000); // 2 second break
		
		do {
			line();
			
			Character fighter = chooseCharacter();
			
			line(enemy + " has " + enemy.getHealth() + " health! (out of " + enemy.getMaxHealth() + ")");
			enemy.setHealth(enemy.getHealth() - fighter.getAttack());
			print(fighter.isPlayer() ? "You attack! " : fighter + " attacks! ");
			Thread.sleep(2000); // 2 second break
            
			if(enemy.getHealth() <= 0){
				line(enemy + " is dead!");
                line();
                line("You gained...");
                
                enemy.getItems().forEach((item, num) -> {
                    line("* " + num + " " + item.getName());
                    main.addItem(item, num);
                });
                
				return;
			}else
				line(enemy + " is now at " + enemy.getHealth() + " health.");
			
            Thread.sleep(2000); // 2 second break
			line((fighter.isPlayer() ? "You have " : fighter + " has ") + fighter.getHealth() + " health! (out of " + fighter.getMaxHealth() + ")");
			fighter.setHealth(fighter.getHealth() - enemy.getAttack());
			line(enemy + " attacks! " + (fighter.isPlayer() ? "You are" : fighter + " is") + " now at " + fighter.getHealth() + " health.");
            Thread.sleep(2000); // 2 second break
			
			if(fighter.isPlayer()){
				if(fighter.getHealth() <= 0){
					line("You died! GAME OVER...");
					System.exit(0);
				}else
					line("You are now at " + fighter.getHealth() + " health.");
			}else{
				if(fighter.getHealth() <= 0){
					line(fighter + " is dead!");
                    main.getCast().remove(fighter);
					continue;
				}else
					line(fighter + " is now at " + fighter.getHealth() + " health.");
				
				
				if(fighter.getLoyalty() <= 0){
					line(fighter + " thinks it's not " + enemy + " that's the problem, it's you!");
					main.getCast().remove(fighter);
					fight(fighter);
				}else if(fighter.getLoyalty() <= 10)
					line(fighter + " has had just about enough of you!");
				else if(fighter.getLoyalty() <= 20)
					line(fighter + " is more then a bit angry.");
				else if(fighter.getLoyalty() <= 30)
					line(fighter + " is getting a bit twitchy.");
                else if(fighter.getLoyalty() <= 40)
					line(fighter + " is uncomfortable.");
			}
            
            Thread.sleep(2000); // 2 second break
		}while(true);
	}
}
