package teameric.zeldomistrust.items;

import teameric.zeldomistrust.entities.Entity;

/**
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider
 */
public class Sword extends Item {

    /**
     * The damage of the sword.
     * @author Eric Schneider
     */
    protected int damage;
	
    /**
     * Constructs the Item object.
     * @param name the name of the object.
     * @param lore the item's lore (information).
     * @param value the item's value.
     * @param damage the Sword's hit damage
     * @author Eric Schneider
     */
    public Sword(String name, String lore, int value, int damage){
		super(name, lore, value);
		
		this.damage = damage;
	}

	@Override
	public String getLore(){
		return "Damage: " + damage + "\n" + lore;
	}
	
    /**
     * Getter method for damage variable.
     * @return Sword's damage
     * @author Eric Schneider
     */
    public int getDamage(){
		return damage;
	}
	
	@Override
	public void applyToEntity(Entity entity){
		if(entity.getSword() == null)
			entity.setSword(this);
		else if(entity.getSword().getDamage() <= getDamage())
			entity.setSword(this);
	}
}
