package teameric.zeldomistrust.items;

import teameric.zeldomistrust.entities.Entity;

/**
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider
 */
public class Item {

    /**
     * The name of the item.
     * @author Eric Schneider
     */
    protected String name;

    /**
     * The item's lore (information).
     * @author Eric Schneider
     */
    protected String lore;

    /**
     * The item's value as a commodity. See Samples in Items.java
     * @author Eric Schneider
     */
    protected int value;
	
    /**
     * Constructs the Item object.
     * @param name the name of the object.
     * @param lore the item's lore (information).
     * @param value the item's value.
     * @author Eric Schneider
     */
    public Item(String name, String lore, int value){
		this.name = name;
		this.lore = lore;
		this.value = value;
	}
	
    /**
     * Gets the name of the Item.
     * @return the name of the Item
     * @author Eric Schneider
     */
    public String getName(){
		return name;
	}
	
    /**
     * Gets the lore of the Item.
     * @return lore of the Item
     * @author Eric Schneider
     */
    public String getLore(){
		return lore;
	}
	
    /**
     * Gets the item's value as a commodity.
     * @return value as a commodity
     * @author Eric Schneider
     */
    public int getValue(){
		return value;
	}
	
    /**
     * Sets the item's name.
     * @param name the name to be set
     * @author Eric Schneider
     */
    public void setName(String name){
		this.name = name;
	}
	
    /**
     * Sets the item's lore.
     * @param lore the lore to be set
     * @author Eric Schneider
     */
    public void setLore(String lore){
		this.lore = lore;
	}
	
    /**
     * Sets the value of the item.
     * @param value the value to be set
     * @author Eric Schneider
     */
    public void setValue(int value){
		this.value = value;
	}
	
    /**
     * Called when added to the inventory of an entity (inside Entity.java).
     * @param entity the entity object that this item is added to.
     * @author Eric Schneider
     */
    public void applyToEntity(Entity entity){ }
	
    /**
     * Test if the items are equal.
     * @param other the other item that is tested against
     * @return true if they are equal, false otherwise
     * @author Eric Schneider
     */
    public boolean equals(Item other){
		return name.equals(other.getName());
	}
}
