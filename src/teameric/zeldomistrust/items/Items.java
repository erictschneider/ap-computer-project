package teameric.zeldomistrust.items;

/**
 * Container class containing Items used within the game.
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/30/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Eric Schneider, Dwight Collier
 */
public class Items {
	// Basic items (lowest value -> highest value)

    /**
     * The bronze coin item
     * @author Eric Schneider
     */
	public static Item bronzeCoin = new Item("Bronze Coin", "A bronze coin. Somewhat valueable, I guess.", 10);

    /**
     * The silver coin item
     * @author Eric Schneider
     */
    public static Item silverCoin = new Item("Silver Coin", "A silver coin. Pretty valueable.", 100);

    /**
     * The gold coin item
     * @author Eric Schneider
     */
    public static Item goldCoin = new Item("Gold Coin", "A golden coin. Very valueable.", 1000);
	
	// Basic swords (lowest damage -> highest damage)

    /**
     * The gold sword item
     * @author Eric Schneider
     */
	public static Sword goldSword = new Sword("Gold Sword", "A golden sword. Very valueable, but not really meant for combat.", 10_000, 5);

    /**
     * The bat item
     * @author Eric Schneider
     */
    public static Sword bat = new Sword("Bat", "A bat, made out of wood. Why do you have it?", 10, 5);

    /**
     * The silver sword item
     * @author Eric Schneider
     */
    public static Sword silverSword = new Sword("Silver Sword", "A silver sword. Somewhat valueable, but not really meant for combat.", 1000, 10);

    /**
     * The bronze sword item
     * @author Eric Schneider
     */
    public static Sword bronzeSword = new Sword("Bronze Sword", "A bronze sword. Decent for combat.", 100, 30);

    /**
     * The iron sword item
     * @author Eric Schneider
     */
    public static Sword ironSword = new Sword("Iron Sword", "An iron sword. Decent for combat.", 100, 30);

    /**
     * The estoc item
     * @author Dwight Collier, Eric Schneider
     */
    public static Sword estoc = new Sword("Estoc", "An estoc. Good for use if you are a fencer.", 100, 35);

    /**
     * The claw item
     * @author Dwight Collier, Eric Schneider
     */
    public static Sword claw = new Sword("Werewolf Claw", "Dapa’s claws. Great for ripping apart enemies.", 1000, 40);

    /**
     * The steel sword item
     * @author Eric Schneider
     */
    public static Sword steelSword = new Sword("Steel Sword", "A steel sword. Steel actually hasn't been invented yet in this time period, so that's cool.", 1000, 50);

    /**
     * The diamond sword item
     * @author Eric Schneider
     */
    public static Sword diamondSword = new Sword("Diamond Sword", "A diamond sword. This is about as good as it gets.", 10_000, 100);

    /**
     * The DP item
     * @author Dwight Collier, Eric Schneider
     */
    public static Sword dp = new Sword("Daily Prompt", "As always its the first item on the agenda... and the last thing your enemies will see.", 10_000, 150);

    /**
     * The Eric sword item
     * @author Eric Schneider
     */
    public static Sword ericSword = new Sword("Eric Sword", "An Eric sword, made of the essence of the coding gods themselves. If you got this, you probably cheated.", 100_000_000, 10_000);
}
