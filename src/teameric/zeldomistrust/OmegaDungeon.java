package teameric.zeldomistrust;

import static teameric.zeldomistrust.ZeldoMistrust.*;
import static teameric.zeldomistrust.Console.*;
import teameric.zeldomistrust.entities.Characters;
import teameric.zeldomistrust.entities.Monsters;

/**
 * The Omega Dungeon, the last dungeon.
 * 
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/31/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Zachary Stanelle, Eric Schneider
 */
public class OmegaDungeon {

    /**
     * @author Zachary Stanelle, Eric Schneider
     * @throws java.lang.InterruptedException should not throw
     */
    public static void play() throws InterruptedException {
        line("You enter the room that had the sign omega dungeon but really it is a long hallway with people squabling talking about different back stories.");
        Thread.sleep(3000);
        line("They all approach you asking you the same question everyone seems to be asking you.");
        Thread.sleep(3000); 
        line("Can I join?...");
        Thread.sleep(3000);
        
        line(Characters.cheftzi.getLore());
        if(confirm("Will you allow Cheftzi to continue her quest?")){
            main.addToCast(Characters.cheftzi);
            line("\"I will not let you down. For the tribe!!!\"");
        }else{
            line("Please just take him down for me then. For my tribe.");
        }
        
        line("Can I join?...");
        Thread.sleep(3000);
        line(Characters.bludsplaz.getLore());
        
        if(confirm("Will you allow Bludsplaz to continue down the road with you?")){
            main.addToCast(Characters.bludsplaz);
            line("Yes, I promise not to suck your blood");
            line("Now time for some vampiric lore.");
            line(Characters.bludsplaz.getLore());
            Thread.sleep(13_000);
        }else
            line("Be gone you child looking man. I have no time to talk to you.");
            
        line("A werewolf named Dapa asks if he can join.");
        Thread.sleep(3000);
        
        if(confirm("Is Dapa and his werewolf genetics enough for him to earn a spot in your party?")){
            main.addToCast(Characters.dapa);
            line("I will firghten away all the enemies before they can even attack.");
            line("Allow me to tell you a scary story *Howl*");
            line(Characters.dapa.getLore());
            Thread.sleep(13_000);
        }else
            line("I do not often get told no. Luckily for you I do not harm humans... usually");
            
        line("Wow, it those people were desperate to be recruited, it's almost as if the devs were on a time crunch.");
        line("You see a sign that says this is definitely the Omega dungeon for real this time.");
        Thread.sleep(6000);
        
        if(main.getCast().contains(Characters.cheftzi)){
            line("Cheftzi instantly walks into spike pit and dies.");
            main.getCast().remove(Characters.cheftzi);
            line("The whole party just met him and simply does not care that he died.");
        }
            
        line("You see a moody looking teen.");
        line("So like you gonna let me join or uh what.");
         if(confirm("Should this teen join you")){
            main.addToCast(Characters.vesper);
            line("\"Thanks I guess\"");
            line(Characters.vesper.getLore());
            Thread.sleep(13_000);
        }else
            line("\"Whatever I did not even want to join anyways.\"");
            
        
        line("You see many monsters in this room. Time to throw down!");
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        fight(Monsters.orcGuard.copy());
        
        if(main.getCast().contains(Characters.dope))
            line("Dang yo, the devs sending 9 orcs at us. Do they not know where are gonna be at the Demon King soon");
        
        line("Another door is made clear to you. A sign that reads maybe this is the lair of Apocalypse who really knows.");
        Thread.sleep(3000);
        line("It was not. Instead you see two people sitting in this room. You already know what they are gonna ask as they approach you.");
        line("Is boolean Marshall(join) party true or false?");
        
        if(confirm("Should Mr.Marshall join?")){
            main.addToCast(Characters.oschuan);
            line("\"Very good that was the right answer\"");
            line(Characters.oschuan.getLore());
            Thread.sleep(13_000);
        }else
            line("\"Ok but just remember that all coding assignments are due tonight by midnight.\"");
        
        line("Welcome to my lair");
        Thread.sleep(3000);
        line("I can tell from your long pause you are still trying to figure out if I am indeed the one you have been searching for.");
        Thread.sleep(3000);
        line("Yes, I am the Demon Lord the one you call Apocalypse. No matter my name I am all powerful. Now tell me why are you hear to bother me.");
        
        if(main.getCast().contains(Characters.albina)){
            line("Albina: I love the people of mother russia. And I will prove myself to them and show them my strength is real.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.grigorii)){
            line("Grigorii: You destoryed something so sacred and special to my people. It can truly never be replaced. Now I must make sure you can never be put back together like the shrine.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.dipti)){
            line("Dipti: I am here to get the good stuff I know you have... I mean for redemption to earn back my family.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.scourge)){
            line("Scourge: You are the reason I have this ridiculous name. My parents would have stopeed this from hapening. Now I must avenge this. Seriously do yuo know how hard it is to get a job when the name Scourge is on the aplication.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.dope)){
            line("Dope:(rapping) You ruined my good time, so now I am gonna spit at you these fire rhymes, you wrecked my house like a child not you gonna get hit with these bars and it will not be mild.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.fritzi)){
            line("Fritzi: I did everything for the fans and you scared them all away. My whole life purpose now gone. Well I will be getting back those fans so it is time to show you what thi german engineered man can do.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.judoc)){
            line("Judoc: You killed the only thing I cared about in this world. He was everything to me now you will die.");
            Thread.sleep(3000);
        }
        if(main.getCast().contains(Characters.bile)){
            line("Bile: You took all my food. Now I am gonna get it back.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.maata)){
            line("Maata: You take things way to far my guy. Leave the mischief to me.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.noa)){
            line("Noa: Destroying my homeland!!! You have some nerve. I will get revenge for what you have done to JAPAN");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.zeno)){
            line("Zeno: My jewls, my jewls you took my jewls!!!!!!!!");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.pika)){
            line("Pika: I will prove myself. Beating you may take some help but I will do it.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.bludsplaz)){
            line("Bludsplaz: I will prove myself. Beating you may take some help but I will do it.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.dapa)){
            line("Dapa: Nothing in this world should be scarier than me. I mean that just simply is not right. Time to become the scariest thing in the world once again.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.oschuan)){
            line("Oschuan: You dare cheat. You dare threaten coding. My goodness you have no clue what is coming for you.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.vesper)){
            line("Vesper: I am the moddy and dramatic one around here. Like seriously stop being like this.");
            Thread.sleep(3000);
        }
        
        if(main.getCast().contains(Characters.paco)){
            line("Paco: YOU KILLED JUAN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            Thread.sleep(3000);
        }
        
        line("All of you and your stupid ideals. Come, face me mortals.");
        fight(Monsters.demonKing.copy());
        line("You have won. You and your party have taken down this absolutely dastardly charecter that is the Demon King. I hope you are proud of yourself. You really really should.");
    }
}