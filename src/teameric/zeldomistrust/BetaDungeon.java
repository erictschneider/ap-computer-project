package teameric.zeldomistrust;

import static teameric.zeldomistrust.ZeldoMistrust.*;
import static teameric.zeldomistrust.Console.*;
import teameric.zeldomistrust.entities.Characters;
import teameric.zeldomistrust.entities.Monsters;

/**
 * The Beta Dungeon, the second dungeon.
 * 
 * <ul>
 * <li>Purpose: Final project</li>
 * <li>Author: Eric Schneider, Dwight Collier, Zachary Stanelle</li>
 * <li>Date: 5/31/2018</li>
 * <li>I have neither given nor received any unauthorized aid on this work.</li>
 * </ul>
 *
 * @author Dwight Collier, Eric Schneider
 */
public class BetaDungeon {

    /**
     * @author Dwight Collier, Eric Schneider
     * @throws java.lang.InterruptedException should not throw
     */
    public static void play() throws InterruptedException {
        line("Welcome to the Beta dungeon");
        line("This is the Demon Lord's second chamber");
        Thread.sleep(4000); //4 second pause
        line();
        line("You look around and there is an emptiness...");
        line("You see nothing around you.");
        Thread.sleep(2000); //2 second pause
        line();
        line("You decide to move foward, deeper into the dungeon.");
        line("You see oddly enough a German wrestler going throughout the cave.");
        Thread.sleep(4000); //4 second pause
        line();
        line("He notices you and begins to walk over.");
        line("\"Hello friend, are you here for the demon lord as well?\"");
        Thread.sleep(2000);
        line();
        line("You tell him that you are and the man smiles.");
        line("\"Then you can watch the great FRITZI VRENI take him out\"");
        Thread.sleep(2000);
        line();
        if(main.getCast().contains(Characters.dipti))
            line("Dipti looked a Frenzi in confusion. \"I've never heard of this tree before.\"");
        else
            line("Confused, you ask who he is.");
        
        line("\"You don't know me?!? You have much to learn about!!\"");
        Thread.sleep(4000);
        line();
        line(Characters.fritzi.getLore());
        Thread.sleep(15_000); // 15 second pause
        line();
        line();
          
        if(confirm("\"Now that you know of me, let's go and bring pain!!\"")){
            main.addToCast(Characters.fritzi);
            line("\"YES, NOW BEHOLD FRITZI VRENI!!\"");
        }else
            line("\"I see so you don't have good taste... oh well.\"");
        
        line("After talking with Fritzi you notice that bandits have surrounded you!!");
        
         if(main.getCast().contains(Characters.albina))
            line("Albina flexes her muscles \" I'll show these bandits what a REAL woman can do!!\"");
            
        Thread.sleep(4000);
        fight(Monsters.bandit.copy());
        fight(Monsters.bandit.copy());
        fight(Monsters.bandit.copy());
        Thread.sleep(4000);
        line();
        line("You managed to fight off those bandits. You decide to go deeper.");
        line("You look around the dungeon and there is finally a change, but a most unpleasant one.");
        Thread.sleep(3000);
        line();
        line("You see monsters of all kinds, torturing people.");
        
        if(main.getCast().contains(Characters.dope))
            line("Chillin backs up a little. \"Aw h*ll no brah, this sh*t is messed up\"");
            
        line("This is the Demon Lord's work, it is unpleasant and you must do something");
        Thread.sleep(2000);
        line();
        line("You see a woman fighting for her life desperately.");
        line("It seems unlikely that she should win.");
        
        if(confirm("Will you help her?")){
            line("You rush to her aid and ask her if she is hurt.");
            Thread.sleep(2000);
            line("\"Thanks for that, I'm still able to fight, let's take them out!!\"");
            main.addToCast(Characters.judoc);
            fight(Monsters.lizardMan.copy());
            fight(Monsters.lizardMan.copy());
        }else{
            line("Congrats, you used the woman as a distraction to get past the fighting. Really moral of you.");
            
             if(main.getCast().contains(Characters.fritzi))
            line("Fritzi looks at you in disgust. \"You know, I may have misjudged your character a little.\"");
            Characters.fritzi.setLoyalty(50);
        }
        
        if(main.getCast().contains(Characters.judoc)){
            line("\"Thanks, I don't think I would have survived without you.\"");
            line("\"My name is Judoc Gwenneg, I'm sorry there was no time to introduce myself.\"");
            Thread.sleep(2000);
            line();
            line(Characters.judoc.getLore());
            Thread.sleep(2000);
            line();
            line("\"Thanks for saving me, I'll fight by your side.\"");
        }else{
            line("You got away without fighting.");
            line("But as you look behind you see her reaching out to you.");
            Thread.sleep(2000);
            line();
            line("She is then burned by the monsters, you can hear screams of agony.");
        }
        
        Thread.sleep(2000);
        line();
        line("You walk throughout the dungeon, left and right people are being slaughtered.");
        line("You continue on through the dungeon, knowing that the only way to beat the demon lord is foward.");
        Thread.sleep(4000);
        line();
        line("You see a man hiding from the monsters.");
        line("You approach him. At first he is startled but then realizes that you are normal.");
        Thread.sleep(4000);
        line();
        line("\"Thank goodness you aren't one of those things, I thought I had been caught");
        line("They capture people and bring them here just to toruture them for amusement.");
        
        if(main.getCast().contains(Characters.grigorii))
            line("Grigorii shudders. \"How horrible, these demons are well...DEMONS!!\"");
        
        Thread.sleep(6000);
        line();
        line("You tell him that you are here to stop the Demon Lord");
        line("\"I hope you can do something about the Demon Lord Apocalypse... life has become worse with him around");
        Thread.sleep(4000);
        line();
        line("There's a way through this dungeon, to the Omega dungeon, his lair. I can le-\"");
        line("Just as the man is about to finish his sentence a red colored lizard man chops his head off");
        
        if(main.getCast().contains(Characters.dipti))
            line("Dipti looks in terror \"I really hope that tree dying was just another hallucination!!\"");
        
        Thread.sleep(6000);
        line();
        line("The lizard man looks at you with a cruel gaze.");
        line("\"I thought I only saw one cowering human but now look at what I dound");
        line("I guess you humans are pretty good at hiding and running.");
        line("I love it when humans run, that makes the chase so much more fun!!");
        line("I'll kill you slow....\"");
        Thread.sleep(12_000);
        fight(Monsters.lizardMan.copy());
        fight(Monsters.lizardMan.copy());
        fight(Monsters.lizardCaptain.copy());
        line();
        line("You managed to fight off the Lizard Captain.");
        line("You look ahead and see that admist the chaos of the dungeon, there is a rather large man boiling in a pot.");
        Thread.sleep(4000);
        line();
        line("\"Help me!!\" he screams.");
         line("You help him out with, more lots of effort and will power.");
        line("\"Thanks brother, I thought I would have been a stew for demons.\"");
        line("You tell him that it was no problem (even though your arms still hurt)");
        line("You tell him that you are after the Demon Lord.");
        line("\"Oh so you are after him too! Let me join you brother!\"");
        Thread.sleep(14_000);
        line();
        
        if(confirm("Will you let him join?")){
            main.addToCast(Characters.bile);
            line("\"Pleasure working with you brother!! The name's Bile Skanda!!\"");
            line("\"Let me tell you about myself!\"");
            Thread.sleep(4000);
            line();
            line(Characters.bile.getLore());
        }else
            line("\"Well I guess that's probably for the best, I'll try to head home I guess.\"");
            
        Thread.sleep(4000);
        line();
        line("You continue your adventure. As you progress you realize that you are lost.");
        line("You need to figure out how to get to the Omega dungeon.");
        line("You wander almost aimlessly until you bump into a pretty short girl");
        Thread.sleep(4000);
        line();
        line("\"Oh hey friend!! Watcha doin'\"");
        line("Considering everything thats happening she's pretty calm...");
        line("You tell her that you are looking for a path to the Omega dungeon");
        line("\"Ohhhhh the Omega dungeon! I don't know but I do know that a girl named Noa knows!!\"");
        line("\"I'll lead you to her!!\"");
        Thread.sleep(7000);
        line();
        line("She then told you that her name was Maata, and lead you to a woman who was fighting lizardmen.");
        line("\"Theeeeere's Noa!! Have fun!!\"");
        
        if(main.getCast().contains(Characters.grigorii))
            line("Scourge protests saying \"THIS ISN't FUN!!\"");
            
        line("Not only is someone in danger, but the only way to the Omega dungeon is in danger. Prepare to fight!!");
        Thread.sleep(7000);
        fight(Monsters.lizardMan.copy());
        fight(Monsters.lizardMan.copy());
        line();
        line("You successfully defended Noa.");
        line("You just saved Noa, so maybe you expect a thank you, but...");
        line("\"I didn't ask for your help amatuer, and I also thought I told you to go away Maata.\"");
        line("You asked Noa if she knew the way to the Omega dungeon.");
        line("\"No, I have no idea where that would even be, I'm itching to find it though.\"");
        Thread.sleep(10_000);
        line();
        line("You ask Maata what the deal was.");
        line("\"Oh I'm soooo sorry, did I say Noa? I meant my good pal Zeno knows where it is!!\"");
        line("A little frustrated, you then begin to walk with Maata when Noa stops you.");
        line("\"You are after the Demon Lord aren't you?\"");
        line("\"I usually go solo but working with another may prove useful.\"");
        line();
        Thread.sleep(10_000);
        
        if(confirm("Will you let Noa join you")){
            main.addToCast(Characters.noa);
            line("\"I see, so you made the right choice.\"");
            line("\"Since we are now comrades let me tell you my story.\"");
            Thread.sleep(4000);
            line();
            line(Characters.noa.getLore());
            Thread.sleep(15_000);
        }else{
            line("\"Very well, I didn't need to team up with an amatuer anyways.\"");
            Thread.sleep(2000);
        }
        
        line();
        line("You continue your journey, following Maata, your only lead.");
        line("Maata leads you to a man who is being held captive by Dark knights.");
        line("\"Oh noes!! Zeno is in trouble!! Guess you gotta save him!!\"");
        line("The dark knights prepare for battle.");
        Thread.sleep(6000);
        fight(Monsters.darkKnight.copy());
        fight(Monsters.darkKnight.copy());
        line();
        line("You freed Zeno!! You then ask him where the Omega dungeon is.");
        line("\"The Omega dungeon, I don't know where that is, does it have fancy stuff?\"");
        line("You give Maata a look that lets her know that you aren't happy.");
        line("\"Hey, Hey, stop staring daggers at me!!\"");
        line("\"Honest mistakes, I'm telling you!! Zeno doesn't know but Pika knows for sure!!\"");
        line("You are starting to see a trend here but you have no other leads on the Omega dungeon.");
        Thread.sleep(12_000);
        line();
        line("Irritated you follow Maata until Zeno stops you.");
        line("\"Your going after the thieving lord aren't you?\"");
        line("You correct 'Thieving' for 'demon' but Zeno insists that he is a thief");
        line("I want to take that thief out, so please let me join you!!");
            
        if(confirm("Will you let Zeno join you?")){
            main.addToCast(Characters.zeno);
            line("\"Thanks!! No Thief will stop us!!\"");
            line("\"Let me tell you more about myself.\"");
        }else
            line("\"But how will I ever reclaim my stolen goods?!\"");
            
            
        line("You follow Maata to what you assume is the next battle.");
        line("And to your suprise, you find Pika and he is just fine!!");
        line("\"Look that's Pika, aaaaand there are no monsters!!");
        line("You then ask Pika if he knows how to get to the Omega dungeon");
        line("However Pika gives you a weird response.");
        line("\"Kill...Kill.\"");
        
        if(main.getCast().contains(Characters.dipti))
            line("Dipti says confidently. \"Yeah I think Pika is on that good sh*t too.\"");
            
        Thread.sleep(9000);
        line();
        line("You should have known it was too good to be true.");
        line("\"Ohhhhh thats right!! I forgot to mention....Pika's possesed.\"");
        line("Pika looks at you and Maata with a blood lust \"Kill...kill...Kill you.\"");
        Thread.sleep(4000);
        line();
        fight(Monsters.possesedPika.copy());
        line();
        line("You defeated Pika!!");
        line("After you beat him up, Pika began to wake up.");
        line("\"What happend to me?\"");
        
        if(main.getCast().contains(Characters.judoc))
            line("Judoc tries to comfort Pika \"You were under an evil influence, but you are safe now.\"");
        else
            line("You explain to Pika that he was possesed.");
        
        line("\"Oh, well then..Thanks for helping me.\"");
        Thread.sleep(6000);
        line();
        line("\"Let me make it up to you, let me join you. I'm a pretty strong fighter.\"");
             
        if(confirm("Will you let Pika join?")){
            main.addToCast(Characters.pika);
            line("\"Alright, they may have gotten me this time but things will be different!!\"");
            line("\"let me tell you about myself, Im gonna blow your mind!!\"");
            Thread.sleep(2000);
            line();
            line(Characters.pika.getLore());
            Thread.sleep(12_000);
        }else
            line("\"Well I guess I would end up being possesed again....\"");
            
        line();
        line("Now that that was settled you began to give Maata the evil eye.");
        line("\"Okay, look we haven't even asked if Pika knew where the Omega dungeon was!!\"");
        line("Pika, confused said \"The what dungeon?\"");
        
        if(main.getCast().contains(Characters.scourge))
            line("Scourge whispers \" Is anyone else picking up on a trend here.\"");
        
        line("\"Okay so maybe none of them knew buuuut to tell you the truth, I kinda already knew.\"");
        line("Maata then points to a door with the sign Omega dungeon looming over it.");
        line("How did you not notice that before?");
        Thread.sleep(10_000);
        line();
        line("It still didn't make any of this better.");
        line("\"Look I know I gave you an unnecessary goose chase but, it's part of who I am.\"");
        Thread.sleep(2000);
        line();
        line(Characters.maata.getLore());
        Thread.sleep(12_000);
        line();
        line("\"I'm sorry about this trouble but I can make this right...let me join you.\"");
            
        if(confirm("Will you forgive Maata and let her join?")){
            main.addToCast(Characters.maata);
            line("\"Thank you so much, I won't let you down.\"");
        }else
            line("\"I.... I understand.... I'll go away.\"");
            
        line();
        line("Now that you know where the door to the next dungeon is you walk towards it but...");
        Thread.sleep(6000);
        line("A boy wearing black holding a laptop shows up.");
        line("\"Hi my name's Christian Collier, and I am one of the devs!!\"");
        line("\"There would be some troll or lizard man here but...\"");
        line("\"I wanted to be in the game and I couldn't be a playable character soo...\"");
        line("\"I'm going to be a boss instead, but don't worry I won't cheat.....much.\"");
        Thread.sleep(10_000);
        fight(Monsters.christianCollier.copy());
        line();
        line("\"Ouch! You beat me even with my hax on... I mean... I played fair and square.\"");
        line("\"Well I gotta go back to class, so cya.\"");
        line("After the developer left, you proceeded through the door to the Omega dungeon.");
        Thread.sleep(6000);
    }
}