# Zeldo Mistrust
Zeldo Mistrust is a text-based adventure RPG written in Java and created by Eric Schneider, Dwight Collier, and Zachary Stanelle.

## Installation
1. Download all of the necessary files.
2. Go to the `Zeldo Mistrust` folder.
3. Open the project with NetBeans.
4. Run the project (big green button).

## Accessing the Javadocs
1. Download all of the necessary files.
2. Go to the `Zeldo Mistrust` folder.
3. Open the project with NetBeans.
4. Click the "Run" dropdown, and then "Generate Javadoc."

Or it's in the "dist" folder and you can get it there.
